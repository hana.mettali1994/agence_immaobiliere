#AgenceImmobiliere
En : EstateAgency is a website that manages the real estate transactions of its clients, from the sale of property to rental management, including valuation or property search. Its collaborators are generally real estate agents or commercial agents.

Fr : AgenceImmobiliere est un site internet qui gère les transactions immobilières de ses clients, de la mise en vente de biens à la gestion locative, en passant par l’estimation ou la recherche de biens. Ses collaborateurs sont généralement des agents immobiliers ou des agents commerciaux.


## Development environment / Environnement de développement

### Pre-requirement / Pré-requis

* PHP 7.3
* MySQL 8.0
* Symfony CLI

En : You can check the pre-requirement with the following command (from the Symfony CLI):

Fr : Vous pouvez vérifier la pré-requis avec la commande suivante (de la CLI Symfony):

```bash
symfony book:check-requirements
symfony check:requirements
```

### Launch the working environment / Lancer l'environnement de développement

```bash
symfony serve -d
```
